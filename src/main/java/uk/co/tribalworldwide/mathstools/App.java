package uk.co.tribalworldwide.mathstools;

/**
 * Hello world!
 *
 */
public class App {

	public static int fibonnaci(int i) {
		if (i <= 0) {
			return 0;
		}
		if (i == 1) {
			return 1;
		}
		if( i > 25) {
			throw new IllegalArgumentException("Arg > 25 now allowed");
		}
		// Fn = Fn-1 + Fn-2
		return fibonnaci(i - 1) + fibonnaci(i - 2);
	}
}
